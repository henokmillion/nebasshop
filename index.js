const dbConnectionUtil = require('./utils/db-connection.util')
const express = require('express')
const app = express()
const userController = require('./controllers/user.controller')
const orderController = require('./controllers/order.controller')

// connect to database
dbConnectionUtil.connect()

app.get('/user', userController.getUsers)
app.get('/order', orderController.getOrders)



const server = app.listen(process.env.PORT || 3000, () => {
    const host = server.address().address
    const port = server.address().port
    console.log(`app listening on ${host}:${port}`)
})