exports.makeUnit = val => {
    if (val == 0) { 
        return {
            status: 400,
            error: "Bad Input: 0 increment",
            success: false
        }
    } else if (val < 0) {
        val = (val * -1) / val
    } else {
        val /= val
    }
    return val
}