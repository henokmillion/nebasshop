const mongoose = require('mongoose')
const Schema = mongoose.Schema
const config = require('../config')

const UserSchema = new Schema({
    status: {
        type: String,
        default: config.active_status.ACTIVE,
        required: true
    },
    username: {
        type: String
    },
    firstname: {
        type: String,
        required: true
    },
    lastname: {
        type: String,
        required: true
    },
    password: {
        type: String,
        required: true,
        minlength: 8
    },
    email: {
        type: String,
        required: true
    },
    phone: {
        type: String,
        required: true,
        minlength: 13
    },
    avatar: {
        type: String
    },
    role: {
        type: String,
        default: config.user_roles.CUSTOMER
    },
    wishlist: {
        type: Array,
    },
    cart: {
        type: Array,
    },
    purchase_history: {
        type: Array
    }
})

module.exports = mongoose.model('User', UserSchema)