const mongoose = require('mongoose')
const Schema = mongoose.Schema
const config = require('../config')

const OrderSchema = new Schema({
    product: {
        type: String,
        required: true
    },
    amount: {
        type: String,
        required: true
    },
    customer: {
        type: String,
        required: true
    },
    merchant: {
        type: String,
        required: true
    },
    payment_status: {
        type: String,
        required: true,
        default: config.payment_status.BEING_PROCESSED
    },
    order_status: {
        type: String,
        default: config.order_status.BEING_PROCESSED
    },
    added_time: {
        type: Date,
        default: Date.now()
    },
    updated: {
        type: Array
    }
})

module.exports = mongoose.model('Order', OrderSchema)