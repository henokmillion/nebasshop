const mongoose = require('mongoose')
const Schema = mongoose.Schema
const config = require('../config')

const CategorySchema = new Schema({
    status: {
        type: String,
        default: config.active_status.ACTIVE,
        required: true
    },
    name: {
        type: String,
        required: true,
        unique: true
    },
    subcategory: {
        type: Array,
        default: [],
        required: true
    },
    product: {
        type: Array,
        required: true,
        default: [],
        required: true
    },
    icon: {
        type: String
    },
    parent: {
        type: String,
        default: config.root
    }
})

module.exports = mongoose.model('Category', CategorySchema)