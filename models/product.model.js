const mongoose = require('mongoose')
const Schema = mongoose.Schema
const config = require('../config')

const ProductSchema = new Schema({
    status: {
        type: String,
        default: config.active_status.ACTIVE,
        required: true
    },
    refund: {
        type: Boolean
    },
    name: {
        type: String,
        required: true
    },
    manufacturer: {
        type: Object
    },
    price: {
        type: String,
        required: true
    },
    price_range: {
        minimum: {
            type: String
        },
        maximum: {
            type: String
        }
    },
    merchant: {
        type: Object
    },
    on_sale: {
        type: Boolean,
        default: false
    },
    price_change: {
        previous_price: {
            type: String
        },
        new_price: {
            type: String
        }
    },
    designer: {
        type: String
    },
    category: {
        type: String,
        required: true
    },
    description: {
        type: String
    },
    pictures: {
        type: Array
    },
    features: {
        type: Array
    },
    variations: {
        type: Object
    },
    sale_count: {
        type: Number,
        default: 0
    },
    customer: {
        type: Array,
        default: []
    },
    post_status: {
        type: String,
        default: config.post_status.PENDING
    },
    added_time: {
        type: Date,
        default: Date.now()
    },
    last_updated: {
        type: Date,
        default: Date.now()
    },
    added_by: {
        type: Object
    },
    quantity: {
        initial: {
            type: String,
            required: true
        },
        in_stock: {
            type: String,
            required: true
        }
    },
    order: {
        type: Array
    }
})

module.exports = mongoose.model('Product', ProductSchema)