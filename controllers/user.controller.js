const userService = require('../services/user.service')


exports.getUsers = (req, res) => {
    userService.getUsers()
    .then(data => res.status(data.status).json(data))
    .catch(err => res.status(err.status).json(err))
}