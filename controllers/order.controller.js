const orderService = require('../services/order.service')

exports.getOrders = (req, res) => {
    orderService.getOrders()
    .then(data => res.status(data.status).json(data))
    .catch(err => res.status(err.status).json(err))
}