const dbConnectionUtil = require('../utils/db-connection.util')
const Product = require('../models/product.model')
const config = require('../config')
const mongoose = require('mongoose')
const userService = require('./user.service')
const mathUtil = require('../utils/math.util')

// connect to databse
dbConnectionUtil.connect()

// add product
exports.addProduct = product => {
    return new Promise((succeed, fail) => {
        product = new Product(product)
        product.save(err => {
            if (err) {
                fail({
                    status: 500,
                    error: err,
                    success: false
                })
            } else {
                succeed({
                    status: 201,
                    data: product,
                    success: true
                })
            }
        })
    })
}
// get products
exports.getProducts = () => {
    return new Promise((succeed, fail) => {
        Product.find({
            status: config.active_status.ACTIVE
        }, (err, data) => {
            if (err) {
                fail({
                    status: 500,
                    error: err,
                    success: false
                })
            } else {
                succeed({
                    status: 200,
                    data: data,
                    success: true
                })
            }
        })
    })
}
// update product
exports.updateProduct = (id, product) => {
    return new Promise((succeed, fail) => {
        update = product
        // check if price is updated
        Product.findById(id, (err, data) => {
            if (err) {
                fail({
                    status: 500,
                    error: err,
                    success: false
                })
            } else {
                if (product.price) {
                    if (parseFloat(product.price) !== parseFloat(data.price)) {
                        update.price_change = {
                            previous_price: data.price,
                            new_price: product.price
                        }
                        if (parseFloat(product.price) < parseFloat(data.price)) {
                            update.on_sale = true
                        }
                    }
                }
            }
        })

    })
}
// update "in_stock" amount
exports.updateAmountInStock = (id, amount=0, inc=1) => {
    inc = mathUtil.makeUnit(inc)
    amount *= inc
    return new Promise((succeed, fail) => {
        Product.findByIdAndUpdate(id, {
            $inc: {
                "quantity.in_stock": amount
            }
        }, (err, data) => {
            if (err) {
                fail({
                    status: 500,
                    error: err,
                    success: false
                })
            } else {
                succeed({
                    status: 200,
                    data: data,
                    success: true
                })
            }
        })
    })
}
// update sell_count
exports.updateSellCount = (id, amount=0, inc=1) => {
    amount *= inc
    return new Promise((succeed, fail) => {
        Product.findByIdAndUpdate(id, {
            $inc: {
                "sale_count": amount
            }
        }, (err, data) => {
            if (err) {
                fail({
                    status: 500,
                    error: err,
                    success: false
                })
            } else {
                succeed({
                    status: 200,
                    data: data,
                    success: true
                })
            }
        })
    })
}

// check purchase
// TODO: update customer history
// TODO: update product sell_count
// TODO: update product amount.in_stock

// remove product
exports.removeProduct = id => {
    return new Promise((succeed, fail) => {
        Product.findByIdAndUpdate(id, 
        {
            status: config.active_status.INACTIVE
        }, (err, data) => {
            if (err) {
                fail({
                    status: 500,
                    error: err,
                    success: false
                })
            } else {
                succeed({
                    status: 200,
                    error: err,
                    success: true
                })
            }
        })
    })
}