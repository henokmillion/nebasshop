const Category = require('../models/category.model')
const mongoose = require('mongoose')
const config = require('../config')

// add a category
exports.addCategory = category => {
    return new Promise((succeed, fail) => {
        category = new Category(category)
        category.save(err => {
            if (err) {
                fail({
                    status: 500,
                    error: err,
                    success: false
                })
            } else {
                succeed({
                    status: 201,
                    data: category,
                    sucecss: true
                })
            }
        })
    })
}

// get categories
exports.getCategories = () => {
    return new Promise((succeed, fail) => {
        Category.find({
            status: config.active_status.ACTIVE
        }, (err, data) => {
            if (err) {
                fail({
                    status: 500,
                    error: err,
                    success: false
                })
            } else {
                succeed({
                    status: 200,
                    data: data,
                    sucecss: true
                })
            }
        })
    })
}

// update category
exports.updateCategory = (id, category) => {
    return new Promise((succeed, fail) => {
        Category.findByIdAndUpdate(id, { ...category
        }, (err, data) => {
            if (err) {
                fail({
                    status: 500,
                    error: err,
                    success: false
                })
            } else {
                succeed({
                    status: 201,
                    data: data,
                    sucess: true
                })
            }
        })
    })
}

// remove a category
exports.removeCategory = id => {
    Category.findByIdAndUpdate(id, {
        status: config.active_status.INACTIVE
    }, (err, data) => {
        if (err) {
            fail({
                status: 500,
                error: err,
                success: false
            })
        } else {
            succeed({
                status: 200,
                data: data,
                sucess: true
            })
        }
    })
}