const dbConnectionUtil = require('../utils/db-connection.util')
const User = require('../models/user.model')
const config = require('../config')
const mongoose = require('mongoose')

// connect to databse
dbConnectionUtil.connect()

// get users
exports.getUsers = () => {
    return new Promise((succeed, fail) => {
        User.find({
            status: config.active_status.ACTIVE
        }, (err, data) => {
            if (err) {
                fail({
                    status: 500,
                    error: err,
                    success: false
                })
            } else {
                succeed({
                    status: 200,
                    data: data,
                    success: true
                })
            }
        })
    })
}

// add new user
exports.addUser = user => {
    return new Promise((succeed, fail) => {
        user = new User(user)
        user.save(err => {
            if (err) {
                fail({
                    status: 500,
                    error: err,
                    success: false
                })
            } else {
                succeed({
                    status: 201,
                    data: user
                })
            }
        })
    })
}

// update user
exports.updateUser = (id, user) => {
    return new Promise((succeed, fail) => {
        User.findByIdAndUpdate(id, { ...user
        }, (err, data) => {
            if (err) {
                fail({
                    status: 500,
                    error: err,
                    success: false
                })
            } else {
                succeed({
                    status: 201,
                    data: data,
                    sucess: true
                })
            }
        })
    })
}

// remove a user
exports.removeUser = id => {
    return new Promise((succeed, fail) => {
        User.findByIdAndUpdate(id, {
            status: config.active_status.INACTIVE
        }, (err, data) => {
            if (err) {
                fail({
                    status: 500,
                    error: err,
                    success: false
                })
            } else {
                succeed({
                    status: 201,
                    data: data,
                    sucess: true
                })
            }
        })
    })
}

// add to cart
exports.addToCart = (userId, product) => {
    return new Promise((succeed, fail) => {
        User.findByIdAndUpdate(id, {
            $push: {
                cart: product
            }
        }, (err, data) => {
            if (err) {
                fail({
                    status: 500,
                    error: err,
                    success: false
                })
            } else {
                succeed({
                    status: 200,
                    data: data,
                    success: true
                })
            }
        })
    })
}

// add to wishlist
exports.addToWishlist = (userId, product) => {
    return new Promise((succeed, fail) => {
        User.findByIdAndUpdate(id, {
            $push: {
                wishlist: product
            }
        }, (err, data) => {
            if (err) {
                fail({
                    status: 500,
                    error: err,
                    success: false
                })
            } else {
                succeed({
                    status: 200,
                    data: data,
                    success: true
                })
            }
        })
    })
}