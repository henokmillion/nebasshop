const dbConnectionUtil = require('../utils/db-connection.util')
const Order = require('../models/order.model')
const config = require('../config')
const mongoose = require('mongoose')

// connect to databse
dbConnectionUtil.connect()

// add order
exports.addOrder = order => {
    return new Promise((succeed, fail) => {
        order = new Order(order)
        order.save(err => {
            if (err) {
                fail({
                    status: 500,
                    error: err,
                    success: false
                })
            } else {
                succeed({
                    status: 201,
                    data: order,
                    success: true
                })
            }
        })
    })
}
// get orders
exports.getOrders = (order_status = config.order_status.BEING_PROCESSED) => {
    return new Promise((succeed, fail) => {
        Order.find({}, (err, data) => {
            if (err) {
                fail({
                    status: 500,
                    error: err,
                    success: false
                })
            } else {
                succeed({
                    status: 200,
                    data: data,
                    success: true
                })
            }
        })
    })
}
// update order
exports.updateOrder = (id, order) => {
    return new Promise((succeed, fail) => {
        if (order.order_status) {
            // business logic
        }
        Order.findByIdAndUpdate(id, { ...order
            },
            (err, data) => {
                if (err) {
                    fail({
                        status: 500,
                        error: err,
                        success: false
                    })
                } else {
                    succeed({
                        status: 200,
                        data: data,
                        success: true
                    })
                }
            })
    })
}
// cancel order
exports.cancelOrder = id => {
    return new Promise((succeed, fail) => {
        if (!mongoose.Types.ObjectId.isValid(id)) {
            fail({
                status: 404,
                error: "order has not been found",
                success: false
            })
        } else {
            Order.findOneAndUpdate({
                order_status: config.order_status.BEING_PROCESSED,
                _id: mongoose.Types.ObjectId(id)
            }, {
                order_status: config.order_status.CANCELLED
            }, (err, data) => {
                if (err) {
                    fail({
                        status: 500,
                        error: err,
                        success: false
                    })
                } else {
                    succeed({
                        status: 200,
                        data: data,
                        success: true
                    })
                }
            })
            // check if product has a refund
                // do refund
        }
    })
}