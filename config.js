module.exports = {
    "database": {
        "local": "mongodb://127.0.0.1:27017/nebasshop-db",
        "remote": "mongodb://feleqech:whachizzle1@ds235251.mlab.com:35251/shop-bot"

    },
    "root": "/",
    "post_status": {
        "PENDING": "pending",
        "ACCEPTED": "accepted",
        "REJECTED": "rejected"
    },
    "order_status": {
        "BEING_PROCESSED": "being_processed",
        "COMPLETED": "completed",
        "CANCELLED": "cancelled" 
    },
    "user_roles": {
        "SUPERVISOR": "supervisor",
        "MERCHANT": "merchant",
        "ADMIN": "admin",
        "CUSTOMER": "customer"
    },
    "payment_status": {
        "BEING_PROCESSED": "being_processed",
        "ACCEPTED": "accepted",
        "COMPLETED": "completed",
        "FAILED": "failed"
    },
    "active_status": {
        "ACTIVE": "active",
        "INACTIVE": "inactive"
    }
}